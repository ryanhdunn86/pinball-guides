This project aims to provide quick summary guides on how to play pinball machines primarily from a competition perspective. Something similar to instruction cards you might see on the machines but a little bit more practical. If you only had 30 - 60 seconds to explain what to go for if someone has never played the game before that's what a guide should try and accomplish.

Website available at: https://singleballplay.com/pinball-guides/

Works offline if you have a recent Android device.

This project was inspired by playing in the Pinburgh tournaments and at the ReplayFX Festival ([replayfx.org](http://replayfx.org)) but can hopefully be useful in general as well. Good luck flipping.

# Adding Guides

Guides are located in the machines directory and are Markdown files, but can generally just be normal text too.

Pull requests with new files and changes are welcome but creating an issue with the name of the machine and the guide in the issue body is OK too for those that aren't familiar with pull requests.

The list of machines that are searchable comes from the machines.json file in the db directory. In addition to adding a guide, if the machine isn't already listed, it will need to be added to the machines.json file.

Try and keep the guides are short as possible. Some games reduce down to "shoot the lit spinner" so say that and explain how to light the spinner and that's enough. Some games might have a few strategies and that's OK to explain too, start with the most basic one first and then explain some alternatives.
