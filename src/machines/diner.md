<a href="https://pintips.net/games/138" class="pintips">view pintips</a>

Right ramp spells DINER lights lock and allows you to spin the cup on the ramp. Generally perfer not to spin the cup as the DINER remains lit after multiball ends and allows quicker path back to multiball, only one shot to the ramp then relights locks.

Easier to start multiball from hole on the right, or you can shoot the spinner and go up top.

Left ramp is worth increasing value on sequential shots on a timer to reset.

Top lanes increase multiplier.
