<a href="https://pintips.net/games/153" class="pintips">view pintips</a>

Shoot center or left scoop and then left ramp to start modes. Complete bomb targets to finish pie and light triball on right ramp.

During multiball, shoot left orbit to light jackpot. Shoot again to light double jackpot. Collect on right ramp. Complete light pie to light 100m right ramp shot.

Completing hat trick (left targets) while in 2-ball play adds the 3rd ball back. You can do this multiple times in one multiball.

Simply comboing the ramps can be decent points.
