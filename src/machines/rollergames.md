<a href="https://pintips.net/games/43" class="pintips">view pintips</a>

Complet WAR targs to light locks. They can be stacked.
Opponents can steal locks, so perhaps wait until you have multiple ready before cashing them in (or at least enough to start multiball). Locks carry between games as well so before choosing order check locks.
Jackpots can be collected repeatedly so trap up and keep shooting ramp for jackpots during multiball
If sudden death starts, shoot the ramp for millions, timer will reset so you can miss if you recover.
Shooting the left orbit will collect teams.
Locking a ball during Sudden Death will put a new ball into the plunger and can be plunged for 1M.
Right targets will relight kickback
