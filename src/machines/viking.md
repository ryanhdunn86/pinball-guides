<a href="https://pintips.net/games/28" class="pintips">view pintips</a>

Shoot the inline drops on the left to advance bonus and bonus multipliers. Collect 50K each hit to the standup after all drops are down.

Shoot center spinner to advance bonus.

Shoot right orbit to collect bonus in saucer on the left. Can collect repeatedly, does not reset.
