<a href="https://pintips.net/games/10" class="pintips">view pintips</a>

Shoot the spinner.

Dropping letters will increase the spinner value, as will inlanes or start rollovers at the top. But any advances while the spinner is spinning will award new value so it's always beneficial to just shoot the spinner.

Completing side drops can be valuable too for bonus but generally not compared to a good spinner rip.
