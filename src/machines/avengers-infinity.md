<a href="https://pintips.net/games/2341" class="pintips">view pintips</a>

__Modes__
Spin the disc to spell 'strange' and shoot the right ramp to start a gem quest. Try to light a portal lock or get THOR ready before starting them for bigger points.

__Portal Locks__
Completing combos lights portal locks which make the next mode a multiball and double scoring while two balls are in play. 

__Thor Multiball__
Multiple hits to the captive ball will start Thor multiball. Each one requires more t-h-o-r completions to start the multiball again.

__Iron Man Multiball__
Complete lanes to spell i-r-o-n. Lanes change by spinning the portal disc. Shoot the tower to lock 3 balls for iron man multiball.

__Soul Gem__
Complete all the characters (display show info progress) to start limited flip mode. Completing the mode will enable a black order multiball restart.

Shot order 
Right orbit, center ramp right ramp, left orbit, thor captive ball, drop targets, Captain Marvel (ramp/spins), tower
