<a href="https://pintips.net/games/160" class="pintips">view pintips</a>

Shoot four (4) lit shots to start a mode.

Change lit mode by hitting pop bumpers.

Always try to bring in a mode before starting a multiball.

Finish lighting lanes corresponding to side choosen to light shot multiplier for rest of the ball. Good choices for placement are right loop and Prime (center).

__Choosing Decepticons__

Megatron multiball easier. Lock four balls to start. All shots lit for jackpots. Shoot Megatron to relight them at any time. Hit standups to relight locks.

__Choosing Autobots__

Mudflaps and skids is a multiball which can be used to start Prime or Megatron multiball. It can be restarted multiple times until finished.

Megatron MB a bit harder since jackpots are lit for short time, but can be relit hitting Megatron.
