Side targets light multiball scoop on left when 'alien' spelled out. Spinner changes 2x, 3x, 5x playfield multiplier which is active during multiball.

Right inlane lights left spinner for a short period of time.

Lock stays lit between balls.

Big points:
Get into multiball and light spinner and rip it.

Alternate Strategy:
Light spinner and rip it for 10K a spin.

Notes:
- Lock scoop needs a bump coming out or frequently drains left.
- No tilt warning
- Lock shot late on flipper, almost shatz late

